<!DOCTYPE html>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="styles.css">

        
        <title>Developer Assignment</title>
    </head>
    <body>
        
        <div class="container">
            
            <div class="center_div">
                
                <span class="title_name">atro</span>
            
                <form action="" method="POST">          
                
                <div class="form-group">
                    <input class="form-control form_input_field" type="text" placeholder="Your Email" name="emp_name">
                </div>
                <div class="form-group">
                    <input class="form-control form_input_field" type="password" placeholder="Your Password" name="password">
                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                </div>
                <div class="form-group">
                    <button type="button" name="sign_in" class="btn btn-primary" data-toggle="modal" data-target="#myModal">SIGN IN</button> 
                </div>                              
            
        </form>
            
        </div>          
   
<!--Modal with user details-->  

        <div class="modal d-block" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">User Information</h4>
        </div>
      <div class="modal-body">
          
          <div class="container">
            <?php
//                $mysqli = new mysqli('localhost', 'root', '', 'dev_assignment_db') or die(mysqli_error($mysqli));

//                $result = $mysqli->query("SELECT * FROM employee WHERE email='abc'") or die($mysqli->error);          

//                if(count($result) == 1){
//                    $row = $result->fetch_array();
//
//                    $emp_name = $row['name'];
//                    $emp_add = $row['address'];
//                    $emp_email = $row['email'];
//                }
            
                    $emp_name = 'Nisansala Ranasinghe';
                    $emp_add = 'Kandy';
                    $emp_email = 'nssala@gmail.com';
              
                ?>
              <div class="form-group">
                  <?php  echo 'User Name:' . ' ' . $emp_name; ?>
              </div>
              <div class="form-group">
                  <?php  echo 'User Address:' . ' ' . $emp_add; ?>
              </div>
              <div class="form-group">
                  <?php  echo 'User Email:' . ' ' . $emp_email; ?>
              </div>  
            
        </div>
          
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Accept</button>
          </div>
          
      </div>
               
      
    </div>
  </div>
</div>
  
</div>
    </body>
</html>
